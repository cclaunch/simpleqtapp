all: gui

gui:
	@mkdir -p build
	@cd build && cmake ..
	@cd build && make

clean:
	@rm -rf build

spotless: clean
	@rm -rf bin lib

