#ifndef SIMPLEQTWINDOW
#define SIMPLEQTWINDOW

#include <QMainWindow>
#include <QLabel>
#include <QVBoxLayout>

class SimpleQtWindow : public QMainWindow
{
	Q_OBJECT
public:
	SimpleQtWindow();
private:
	QWidget *centralWidget;
};

#endif
